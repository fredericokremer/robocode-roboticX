package fatec2018;//Nome padrão do pacote, decidido pelo Professor Ângelo Luz.
import robocode.*;
import java.awt.Color;
import robocode.AdvancedRobot;    
import static robocode.util.Utils.normalRelativeAngleDegrees;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/** Declaração da Classe
 * RoboticxDeadpool, tem como movimentação ir até o centro da Arena
 * Logo após ficar fazendo um circulo e atirando em rôbos proximos
 * @author luans
 */
public class RoboticxDeadpool extends AdvancedRobot
{
        double xRobo, yRobo;//Váriaveis para pegar a posição do rôbo
	
        /** O método run é invocado ao começar o programa. 
            Ele fica em constante execução.
        */
        public void run() {
		
                setBodyColor(Color.red);//Mudança da cor do corpo do rôbo para Vermelho
                setGunColor(Color.black);// Mudança da cor do canhão para Preto
                setRadarColor(Color.red);// Mudança da cor do radar para vermelho
                
                xRobo = getX();// Pegar posição do rôbo no eixo X
                yRobo = getY();// Pegar posição do rôbo no eixo Y
                
                /** Condição para o Rôbo ir ao centro da arena
                 * 1) Gira o corpo do rôbo dependendo de onde ele se localiza no mapa, parte inferior para cima (360) ou parte superiror para baixo (180)
                 * 2) Divide a altura por dois, diminui pela posição que o rôbo se encontra no eixo Y e avança
                 * 3) Vira 90º ou para esquerda, ou para direita dependendo do quadrante que  rôbo se encontra
                 * 4) Divide a altura por dois, diminui 150 pixels(para não ficar bem ao centro do mapa), diminui pela posição que o rôbo se encontra no eixo X e avança
                 */
                if (xRobo < 500 && yRobo < 500) {
                        turnRight(360 - getHeading());// 1)
                        ahead((getBattleFieldHeight()/2) - yRobo);// 2)
                        turnRight(90);// 3)
                        ahead(((getBattleFieldWidth()/2) + 150) - xRobo);// 4)
                        
                    }else if (xRobo < 500 && yRobo > 500) {
                        turnRight(180 - getHeading());
                        ahead(yRobo - (getBattleFieldHeight()/2));
                        turnLeft(90);
                        ahead(((getBattleFieldWidth()/2) + 150) - xRobo);
                        
                    }else if (xRobo > 500 && yRobo > 500) {
                        turnRight(180 - getHeading());
                        ahead(yRobo - (getBattleFieldHeight()/2));
                        turnRight(90);
                        ahead(xRobo - ((getBattleFieldWidth()/2) - 150));
                        
                    }else if (xRobo > 500 && yRobo < 500) {
                        turnRight(360 - getHeading());
                        ahead((getBattleFieldHeight()/2) - yRobo);
                        turnLeft(90);
                        ahead(xRobo - ((getBattleFieldWidth()/2) - 150));
                        
                    }
                /** Loop de movimentação do robô, roda sempre até que o robô 
                    * seja reiniciado ou eliminado.
                    * Dentro dele o rôbo estará executando um loop fazendo uma movimentação em circulo
                */
    		while(true) {
                        setTurnGunRight(180);// Gira o canhão 180º para direita
                        setAhead(800);//Movimenta o rôbo para frente 800 pixels
                        setTurnGunRight(180);// Gira o canhão 180º para direita
                        setTurnRight(270);//Gira o corpo do rôbo em 270º
                        execute();//executa os comandos anteriores
                }
	}
        
        /** Método onScannedRobot
        Este método é invocado toda vez que escaneado um robô adversário ou até mesmo amigo.
        Decide a ação a ser feita após o escanamento.
        Neste método o robô vai abrir fogo contra o inimigo.
        @param inimigo - o que foi escaneado.
        */
	public void onScannedRobot(ScannedRobotEvent inimigo) {
            
            String nomeDoRobo = inimigo.getName();// Variável que irá armazenar o nome do rôbo escaneado
            
            /** Condição para se o nome do rôbo escaneado for igual ao nome dos
             * aliados e do BorderGuard, scanear novamente
             * Se passar por essa condição atirar com fire 2 se o inimigo estiver em uma distancia
             * entre 200 e 100, e atirar com fire 3 se o inimigo estiver com uma distancia menor que 100
             */
            if (nomeDoRobo.equals("fatec2018.RoboticxDemolidor*") || nomeDoRobo.equals("fatec2018.RoboticxHomemDeFerro*") || nomeDoRobo.equals("samplesentry.BorderGuard")){ 
                scan();
            } else {
                if (inimigo.getDistance() < 200 && inimigo.getDistance() > 100) {
                    fire(2);
                }else if (inimigo.getDistance() < 100) {
                    fire(3);    
                }
            }
        }
        /** Método onHitWall
         * Nesse metodo se o rôbo bater na parede
         * andar 100 pixels para traz
         * @param e 
         */
	public void onHitWall(HitWallEvent e) {
            back(100);
        }
}