package fatec2018;
import robocode.*;
import java.awt.Color;
import java.util.*;
import robocode.util.Utils;


// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html
    /** Declaração da classe.
        RoboticxDemolidor - robô fatec2018 By Rogimar.
        Este robô tem movimentação de modo aleatorio, e tenta manter um inimigo scanneado em "target"
    */ 
public class RoboticxDemolidor extends AdvancedRobot
{
 		int turnValue = 45;
	String nome = "";
	Random random = new Random();
	// funcao para deslocar o robo para uma posicao especifica no mapa
	public void goTo(double x, double y){

		x -= getX();
		y -= getY();
	 
		double angleToTarget = Math.atan2(x, y);
		double targetAngle = Utils.normalRelativeAngle(angleToTarget - getHeadingRadians());
	 
		double turnAngle = Math.atan(Math.tan(targetAngle));
		turnRightRadians(turnAngle);
		if(targetAngle == turnAngle) {
			ahead(200);
		} else {
			turnRight(180);
			ahead(200);
		}
	
	};
	// funcao principal
	public void run() {
		
		//cores do robo
		setBodyColor(Color.RED);
		setGunColor(Color.RED);
		setRadarColor(Color.RED);
		setScanColor(Color.RED);
        setBulletColor(Color.white);
 
		//loop
		while(true){
			// ajustar posicao X
			
			goTo((double) random.nextInt(6)*100 + 200, 
                 (double) random.nextInt(6)*100 + 200);
			turnRadarLeft(360);
			scan();
		}
	}
	//caso um robo seja scanneado
	public void onScannedRobot(ScannedRobotEvent e) {
	
		nome = e.getName();
		turnValue = 0;
		//ignorar robos do mesmo time e o "borderguard"
		if (nome.startsWith("fatec2018.Robotic") || 
            nome.startsWith("samplesentry.BorderGuard")){
			return;
		} else {
			//se virar para o alvo
			double angle = -e.getBearing();
			turnLeft(angle);
			if(e.getDistance()>500){
				fire(0.1);
			} else if(e.getDistance()>200){
				fire(1);
			} else {
				fire(2);
			};
			scan();
		};
	}
    //fazer após ser atingido
	public void onHitByBullet(HitByBulletEvent e) {
		goTo((double) random.nextInt(600) + 200, 
             (double) random.nextInt(600) + 200);
	
	}
	
	// fazer após batida de robos
	public void onHitWall(HitWallEvent e){
		goTo((double) random.nextInt(600) + 200, 
             (double) random.nextInt(600) + 200);
	}	


}
